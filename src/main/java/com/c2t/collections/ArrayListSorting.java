package com.c2t.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ArrayListSorting {

	public static void main(String[] args) {
		

		List l = new ArrayList();

		l.add("X");
		l.add("B");
		
		System.out.println(l);
		
		Collections.sort(l);
		
		System.out.println(l);

	}

}
