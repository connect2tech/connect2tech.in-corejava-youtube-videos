package com.c2t.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ArrayVsList {

	public static void main(String[] args) {
		int a[] = { 10, 20 };
		
		for(int i=0;	i<a.length;	i++){
			if(a[i] == 10){
				System.out.println("The value 10 is present in the list...");
				break;
			}else{
				System.out.println("Bad luck. Try another number...");
			}
		}

		List<String> l = new <String>ArrayList();

		l.add("A");
		l.add("B");
		
		System.out.println(l.contains("A"));

	}

}
