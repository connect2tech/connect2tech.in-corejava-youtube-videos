package com.c2t.collections;

import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class HashMapVsTreeMap {
	public static void main(String[] args) {
		Map m = new HashMap();
		m.put(null, null);
		m.put("K1", null);
		m.put("K2", null);
		
		System.out.println(m);
		
		
		Map m2 = new TreeMap();
		m2.put("KKK", null);
		m2.put("LLL", null);
		System.out.println(m2);
	}
}
