package com.c2t.collections;

import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class HastSetVsTreeSet {
	public static void main(String[] args) {
		Set s1 = new HashSet();
		s1.add("A");
		s1.add("B");
		s1.iterator();
		
		Set t1 = new TreeSet();
		t1.add("A");
		t1.add("B");
		
		t1.iterator();
	}
}
