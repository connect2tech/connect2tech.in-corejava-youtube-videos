package com.c2t.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class IteratorVsListIterator {
	public static void main(String[] args) {
		List<String> l = new <String>ArrayList();

		l.add("A");
		l.add("B");
		
		System.out.println(l.contains("A"));
		
		l.iterator();
		l.listIterator();
		
		Set s = new HashSet();
	}
}
