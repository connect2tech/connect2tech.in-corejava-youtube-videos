package com.c2t.collections;

import java.util.ArrayList;
import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ListVsSet {

	public static void main(String[] args) {
		List<String> l = new <String>ArrayList();

		l.add("A");
		l.add("B");
		l.add("A");
		
		System.out.println(l);
		
		l.remove("A");
		System.out.println(l);
		
	}
}
