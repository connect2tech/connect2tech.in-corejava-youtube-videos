package com.c2t.collections;

import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MyArrayList {
	public static void main(String[] args) {
		List <String> l = new <String> ArrayList();
		List <String> l2 = new <String> ArrayList();
		
		l.add("A");
		l.add("B");
		
		
		l2.add("A1");
		l2.add("B1");
		
		l.addAll(l2);
		
		System.out.println(l);
		
	}
}
