package com.c2t.collections;

import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MyArrayListGc {
	public static void main(String[] args) {
		List <String> l = new <String> ArrayList();
		
		String s1 = "A";
		String s2 = "B";
		
		l.add(s1);
		l.add(s2);
		
		l.remove("A");
		
	}
}
