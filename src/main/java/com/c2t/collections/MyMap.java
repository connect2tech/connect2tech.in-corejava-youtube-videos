package com.c2t.collections;

import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MyMap {
	
	 public static void main(String[] args) {
		Map <String, String> m= new <String, String> HashMap();
		
		String k1 = "key1";
		System.out.println("k1.hashCode()="+k1.hashCode());
		
		String k2 = "key2";
		System.out.println("k2.hashCode()="+k2.hashCode());
		
		m.put("k1", "v1");
		m.put("k2", "v1");
		
		Set s = m.keySet();
		
		Iterator <String> iter = s.iterator();
		
		while(iter.hasNext()){
			String key = iter.next();
			String value = m.get(key);
		}
		
		
		
	}

}
