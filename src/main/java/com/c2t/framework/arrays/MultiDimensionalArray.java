package com.c2t.framework.arrays;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MultiDimensionalArray {
	public static void main(String[] args) {
		int arr [][] = {
				
				{10,20},
				{30,40}
				
		};
		
		System.out.println(arr[0][0]);
		System.out.println(arr[1][1]);
	}
}
