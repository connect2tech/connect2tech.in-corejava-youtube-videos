package com.c2t.framework.arrays;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SingleDimensionalArray {
	public static void main(String[] args) {
		int a [] = new int[5];
		int [] b  = new int[5];
		int c [] = {10,20,30,40,50};
		
		for(int i=0;i<5;i++){
			a[i] = i + 10;
			
			System.out.println(a[i]);
		}
		
		System.out.println("--------------");
		
		for(int j=0;j<5;j++){
			System.out.println(c[j]);
		}
	}
}
