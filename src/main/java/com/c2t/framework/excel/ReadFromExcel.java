package com.c2t.framework.excel;

import java.io.*;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ReadFromExcel {
	public static void main(String[] args) {

		// FileInputStream file = new FileInputStream(new
		// File("howtodoinjava_demo.xlsx"));
		
		// XSSFWorkbook workbook = new XSSFWorkbook(file);
		
		// XSSFSheet sheet = workbook.getSheetAt(0);
		// sheet.getSheetName().equals("Employee Data")
		
		// Iterator<Row> rowIterator = sheet.iterator();
		// Row row = rowIterator.next();
		// Iterator<Cell> cellIterator = row.cellIterator();
		
		File f = new File("DDT.xlsx");
		
		try {
			FileInputStream fis = new FileInputStream(f);
			
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet("Employee Data");
			
			Iterator<Row> rows  =sheet.iterator();
			
			while(rows.hasNext()){
				Row row = rows.next();
				
				Iterator<Cell>  cells = row.cellIterator();
				
				while(cells.hasNext()){
					Cell cell = cells.next();
					System.out.println(cell);
				}
				
				System.out.println("--------------------");
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
