package com.c2t.framework.props;

import java.util.Properties;
import java.io.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ReadingFromPropertiesFile {
	public static void main(String[] args) {
		File f = new File("framework.properties");
		Properties  prop = new Properties();
		
		try {
			InputStream is = new FileInputStream(f);
			prop.load(is);
			
			String s = prop.getProperty("dummy");
			System.out.println(s);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}
