package com.c2t.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

abstract class Shape{
	
	Shape(){
		System.out.println("Shape");
	}
	
	void display(){
		System.out.println("display");
	}
	
	abstract public void draw();
}


class Circle extends Shape{
	public void draw(){
		System.out.println("I am circle...");
	}
}

class Triangle extends Shape{
	public void draw(){
		System.out.println("I am Triangle...");
	}
}

public class AbstractConcepts {
	public static void main(String[] args) {
		Shape s = new Circle();
		s.draw();
	
	}
}
