package com.c2t.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

interface RemoteControl{
	static final String color = "black";
	abstract void powerOnOff();
	void volumeIncreaseDecrease();
}

class TVRemote implements RemoteControl {
	public void powerOnOff(){
		System.out.println("powerOnOff");
	}
	
	public void volumeIncreaseDecrease(){
		System.out.println("volumeIncreaseDecrease");
	}
}

public class InterfaceConcepts {
	public static void main(String[] args) {
		RemoteControl r1 = new TVRemote();
		r1.powerOnOff();
		
		System.out.println(RemoteControl.color);
	}
}
