package com.c2t.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Car{
	String color;
	final int fuelCapacity = 20;
	
	public void changeColor(){
		color = "blue";
		//fuelCapacity = 25;
	}
	
	final public void maxSpeed(){
		System.out.println("maxSpeed");
	}
}

class BMW extends Car{
	
}

public class UnderstandingFinal {
	
	public static void main(String[] args) {
		Car c = new Car();
		System.out.println(c.color);
		
		c.changeColor();
		System.out.println(c.color);
	}
}
