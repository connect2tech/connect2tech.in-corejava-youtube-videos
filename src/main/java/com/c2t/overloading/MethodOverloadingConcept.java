package com.c2t.overloading;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Calculator {

	// Method Signature comprises the name of the method, and the types and
	// order of the parameters in the formal parameter list.

	// Several method implementations may have the same name, as long as the
	// method signatures differ.
	int sum(int a, int b) {
		int sum = 0;
		sum = a + b;
		return sum;
	}
	
	
	String sum(String a, String b) {
		String  addString;
		addString = a + b;
		return addString;
	}
	
	int sum(int a, int b, int c) {
		int sum = 0;
		sum = a + b +c;
		return sum;
	}
	
	double min(double a, double b){
		return 10.5;
	}

	float min(float a, float b){
		return 10.5f;
	}
}


class ScientificCalculator extends Calculator{
	String sum(String a, int b) {
		String summation;
		summation = a + b;
		return summation;
	}
}

public class MethodOverloadingConcept {
	
	public static void main(String[] args) {
		Calculator c = new Calculator();
		c.sum(10,20);
		
		c.sum("Hello ", "World ");
		
		ScientificCalculator sc = new ScientificCalculator();
		sc.sum("Method Overloading", 10);
		
	}

}
