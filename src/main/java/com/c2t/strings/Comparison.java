package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Comparison {
	public static void main(String[] args) {
		String s1 = "ABCDE";
		String s2 = "CD";
		
		int val = s1.compareTo(s2);
		System.out.println(val);
	}

}
