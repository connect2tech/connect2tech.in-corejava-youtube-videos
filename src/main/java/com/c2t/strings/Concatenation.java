package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Concatenation {
	
	public static void main(String[] args) {
		String s1 = "Hello";
		int s2 = 10;
		
		String s3 = s1 + s2;
		
		System.out.println(s3);
	}

}
