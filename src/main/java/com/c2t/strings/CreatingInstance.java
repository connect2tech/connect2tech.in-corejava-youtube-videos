package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class CreatingInstance {
	public static void main(String[] args) {
		String s1 = new String("I am string 1");
		
		String s2 = "I am string 2";
		
		String s3 = "I am string 222";
		
		String s4 = new String("I am string 2");
		
		
		if(s2 == s3){
			System.out.println("s2 == s3");
		}else{
			System.out.println("s2 != s3");
		}
		
		if(s2 == s4){//True
			System.out.println("s2 == s4");
		}else{
			System.out.println("s2 != s4");
		}
		
		
		if(s2.equals(s4)){
			System.out.println("s2.equals(s4)");
		}
	}
}
