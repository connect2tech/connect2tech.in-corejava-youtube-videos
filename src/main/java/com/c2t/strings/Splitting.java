package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Splitting {
	public static void main(String[] args) {
		String str = "abcxbcxdd";
		String splitter = "x";
		String[] string = str.split(splitter);
		
		for(int i=0;	i<string.length;	i++){
			System.out.println(string[i]);
		}
		
	}
}
