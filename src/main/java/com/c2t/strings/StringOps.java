package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StringOps {
	
	public static void main(String[] args) {
		/*String s1 = new String("Hello");
		System.out.println(s1.hashCode());
		
		s1 = s1.concat(" Java");
		System.out.println(s1.hashCode());*/
		
		
		StringBuffer sbuf = new StringBuffer("I am buffer");
		System.out.println(sbuf.hashCode());
		
		sbuf.append(" I am modified...");
		System.out.println(sbuf.hashCode());
		
	}

}
