package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SwitchCase {

	public static void main(String[] args) {

		String day = new String("Saturday");
		
		

		switch (day) {

		case "Monday":
			break;
		case "Saturday":
			
			System.out.println("I am sat...");
			
			break;
		default:
			System.out.println("I am default...");
		}

	}

}
