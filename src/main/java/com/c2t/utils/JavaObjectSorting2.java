package com.c2t.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class JavaObjectSorting2 {

	/**
	 * This class shows how to sort primitive arrays, Wrapper classes Object
	 * Arrays
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// sorting object array
		Employee1[] empArr = new Employee1[4];
		empArr[0] = new Employee1(10, "Mikey", 25, 10000);
		empArr[1] = new Employee1(20, "Arun", 29, 20000);
		empArr[2] = new Employee1(5, "Lisa", 35, 5000);
		empArr[3] = new Employee1(1, "Pankaj", 32, 50000);

		// sorting employees array using Comparable interface implementation
		Arrays.sort(empArr);
		System.out.println("Default Sorting of Employees list:\n" + Arrays.toString(empArr));

	}
}