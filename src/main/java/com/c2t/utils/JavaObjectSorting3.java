package com.c2t.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 * 
 * 
 */

class AgeCompare implements Comparator {
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub

		Employee2 e1 = (Employee2) o1;
		Employee2 e2 = (Employee2) o2;

		if (e1.getAge() < e2.getAge()) {
			return 1;
		} else if (e1.getAge() > e2.getAge()) {
			return -1;
		} else {
			return 0;
		}

	}
}

public class JavaObjectSorting3 {

	/**
	 * This class shows how to sort primitive arrays, Wrapper classes Object
	 * Arrays
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// sorting object array
		List list = new ArrayList();
		Employee2 e1 = new Employee2(10, "Mikey", 25, 10000);
		Employee2 e2 = new Employee2(20, "Arun", 29, 20000);
		Employee2 e3 = new Employee2(5, "Lisa", 35, 5000);
		Employee2 e4 = new Employee2(1, "Pankaj", 32, 50000);

		list.add(e1);
		list.add(e2);
		list.add(e3);
		list.add(e4);

		// sorting employees array using Comparable interface implementation

		AgeCompare age = new AgeCompare();

		Collections.sort(list, age);

		System.out.println(list);

	}
}